import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { RNSerialport, definitions, actions } from "react-native-serialport";

export default function App() {
  return (
    <View style={styles.container}>
      <Text>pp!</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
